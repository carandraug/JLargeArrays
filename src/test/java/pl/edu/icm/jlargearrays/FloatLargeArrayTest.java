/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jlargearrays;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class FloatLargeArrayTest extends LargeArrayTest
{

    public FloatLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testFloatLargeArrayEqualsHashCode()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        FloatLargeArray b = new FloatLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setFloat(0, 1f);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testFloatLargeArrayApproximateHashCode()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        FloatLargeArray b = new FloatLargeArray(10);
        a.setFloat(0, 1f);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new FloatLargeArray(10, 0f);
        b = new FloatLargeArray(10, 1f);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testFloatLargeArrayConstant()
    {
        FloatLargeArray a = new FloatLargeArray(1l << 33, 2.5f);
        assertEquals(2.5f, a.getFloat(0), DELTA);
        assertEquals(2.5f, a.getFloat(a.length() - 1), DELTA);
        Throwable e = null;
        try {
            a.setFloat(0, 3.5f);
        } catch (IllegalAccessError ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalAccessError);
    }

    @Test
    public void testFloatLargeArrayGetSet()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        long idx = 5;
        float val = 3.4f;
        a.setFloat(idx, val);
        assertEquals(val, a.getFloat(idx), DELTA);
        idx = 6;
        a.set(idx, val);
        assertEquals(val, a.get(idx), DELTA);
    }

    @Test
    public void testFloatLargeArrayGetSetNative()
    {
        FloatLargeArray a = new FloatLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            float val = 3.4f;
            a.setToNative(idx, val);
            assertEquals(val, (float) a.getFromNative(idx), DELTA);
        }
    }

    @Test
    public void testFloatLargeArrayGetData()
    {
        float[] data = new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f, 8.8f, 9.9f, 10.10f};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        LargeArray.setMaxSizeOf32bitArray(1 << 30);
        FloatLargeArray a = new FloatLargeArray(data);
        float[] res = a.getFloatData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++], DELTA);
        }
    }
}
