/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jlargearrays;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class ObjectLargeArrayTest extends LargeArrayTest
{

    public ObjectLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testObjectLargeArrayEqualsHashCode()
    {
        ObjectLargeArray a = new ObjectLargeArray(10);
        ObjectLargeArray b = new ObjectLargeArray(10);
        a.set(0, 1);
        b.set(0, 1);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.set(0, 2);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
        a.set(0, 3);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testObjectLargeArrayApproximateHashCode()
    {
        ObjectLargeArray a = new ObjectLargeArray(10);
        ObjectLargeArray b = new ObjectLargeArray(10);
        a.set(0, new Double(0));
        b.set(0, new Double(1));
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new ObjectLargeArray(10, new Double(0));
        b = new ObjectLargeArray(10, new Double(1));
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testObjectLargeArrayConstant()
    {
        ObjectLargeArray a = new ObjectLargeArray(1l << 33, new Float(12345));
        assertEquals(12345f, a.get(0));
        assertEquals(12345f, a.get(a.length() - 1));
        Throwable e = null;
        try {
            a.set(0, new Float(12346));
        } catch (IllegalAccessError ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalAccessError);
    }

    @Test
    public void testObjectLargeArrayGetSet()
    {
        ObjectLargeArray a = new ObjectLargeArray(10, 84);
        long idx = 5;
        Double val1 = 2.0;
        Double val2 = Double.MAX_VALUE;
        a.set(idx, val1);
        assertEquals(val1, a.get(idx));
        a.set(idx, val2);
        assertEquals(val2, a.get(idx));
        assertEquals(val2.getClass(), a.getElementClass());
    }

    @Test
    public void testObjectLargeArrayGetSetNative()
    {
        ObjectLargeArray a = new ObjectLargeArray(10, 84);
        if (a.isLarge()) {
            long idx = 5;
            Double val1 = 2.0;
            Double val2 = Double.MAX_VALUE;
            a.setToNative(idx, val1);
            assertEquals(val1, a.getFromNative(idx));
            a.setToNative(idx, val2);
            assertEquals(val2, a.getFromNative(idx));
            assertEquals(val2.getClass(), a.getElementClass());
        }
    }
}
