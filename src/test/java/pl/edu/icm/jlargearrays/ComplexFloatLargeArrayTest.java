/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jlargearrays;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class ComplexFloatLargeArrayTest extends LargeArrayTest
{

    public ComplexFloatLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testComplexFloatLargeArrayEqualsHashCode()
    {
        ComplexFloatLargeArray a = new ComplexFloatLargeArray(10);
        ComplexFloatLargeArray b = new ComplexFloatLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.set(0, new float[]{1f, 2f});
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testComplexFloatLargeArrayApproximateHashCode()
    {
        ComplexFloatLargeArray a = new ComplexFloatLargeArray(10);
        ComplexFloatLargeArray b = new ComplexFloatLargeArray(10);
        a.setComplexFloat(0, new float[]{1f, 2f});
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new ComplexFloatLargeArray(10, new float[]{1f, 2f});
        b = new ComplexFloatLargeArray(10, new float[]{2f, 3f});
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testComplexFloatLargeArrayConstant()
    {
        ComplexFloatLargeArray a = new ComplexFloatLargeArray(1l << 33, new float[]{2.5f, 1.5f});
        assertEquals(2.5f, a.getComplexFloat(0)[0], DELTA);
        assertEquals(1.5f, a.getComplexFloat(0)[1], DELTA);
        assertEquals(2.5f, a.getComplexFloat(a.length - 1)[0], DELTA);
        assertEquals(1.5f, a.getComplexFloat(a.length - 1)[1], DELTA);
        Throwable e = null;
        try {
            a.setComplexFloat(0, new float[]{3.5f, 4.5f});
        } catch (IllegalAccessError ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalAccessError);
    }

    @Test
    public void testComplexFloatLargeArrayGetSet()
    {
        ComplexFloatLargeArray a = new ComplexFloatLargeArray(10);
        long idx = 5;
        float[] val = {3.4f, -3.7f};
        a.setComplexFloat(idx, val);
        assertEquals(val[0], a.getComplexFloat(idx)[0], DELTA);
        assertEquals(val[1], a.getComplexFloat(idx)[1], DELTA);
        idx = 6;
        a.set(idx, val);
        assertEquals(val[0], a.get(idx)[0], DELTA);
        assertEquals(val[1], a.get(idx)[1], DELTA);
    }

    @Test
    public void testComplexFloatLargeArrayGetSetNative()
    {
        ComplexFloatLargeArray a = new ComplexFloatLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            float[] val = {3.4f, -3.7f};
            a.setToNative(idx, val);
            assertEquals(val[0], a.getFromNative(idx)[0], DELTA);
            assertEquals(val[1], a.getFromNative(idx)[1], DELTA);
        }
    }

    @Test
    public void testComplexFloatLargeArrayGetData()
    {
        float[] data = new float[]{1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f, 8.8f, 9.9f, 10.10f};
        int startPos = 1;
        int endPos = 5;
        int step = 2;
        ComplexFloatLargeArray a = new ComplexFloatLargeArray(data);
        float[] res = a.getComplexData(null, startPos, endPos, step);
        int idx = 0;
        for (int i = startPos; i < endPos; i += step) {
            assertEquals(data[2 * i], res[2 * idx], DELTA);
            assertEquals(data[2 * i + 1], res[2 * idx + 1], DELTA);
            idx++;
        }
    }
}
