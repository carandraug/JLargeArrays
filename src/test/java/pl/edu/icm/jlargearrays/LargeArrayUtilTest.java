/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jlargearrays;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class LargeArrayUtilTest extends LargeArrayTest
{

    public LargeArrayUtilTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testLogicLargeArrayArraycopy()
    {

        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LOGIC, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        LogicLargeArray b = new LogicLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(false, b.getBoolean(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getBoolean(srcPos + i), b.getBoolean(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(false, b.getBoolean(i));
        }

        b = new LogicLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getByteData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(false, b.getBoolean(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getBoolean(srcPos + i), b.getBoolean(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(false, b.getBoolean(i));
        }

        byte[] bb = new byte[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new LogicLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(false, b.getBoolean(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getBoolean(srcPos + i), b.getBoolean(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(false, b.getBoolean(i));
        }
    }

    @Test
    public void testLogicLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LOGIC, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i), b.getInt(i));
        }
    }

    @Test
    public void testByteLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.BYTE, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        ByteLargeArray b = new ByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getByte(srcPos + i), b.getByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getByte(i));
        }

        b = new ByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getByteData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getByte(srcPos + i), b.getByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getByte(i));
        }

        byte[] bb = new byte[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ByteLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getByte(srcPos + i), b.getByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getByte(i));
        }
    }

    @Test
    public void testByteLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.BYTE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.LOGIC);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i) != 0 ? 1 : 0, b.getByte(i));
        }
    }

    @Test
    public void testUnsignedByteLargeArrayArraycopy()
    {
        UnsignedByteLargeArray a = (UnsignedByteLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        UnsignedByteLargeArray b = new UnsignedByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getUnsignedByte(srcPos + i), b.getUnsignedByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }

        b = new UnsignedByteLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getUnsignedByteData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getUnsignedByte(srcPos + i), b.getUnsignedByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }

        byte[] bb = new byte[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new UnsignedByteLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getUnsignedByte(srcPos + i), b.getUnsignedByte(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getUnsignedByte(i));
        }
    }

    @Test
    public void testUnsignedByteLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i), b.getInt(i));
        }
    }

    @Test
    public void testShortLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        ShortLargeArray b = new ShortLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getShort(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getShort(srcPos + i), b.getShort(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getShort(i));
        }

        b = new ShortLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getShortData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getShort(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getShort(srcPos + i), b.getShort(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getShort(i));
        }

        short[] bb = new short[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ShortLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getShort(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getShort(srcPos + i), b.getShort(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getShort(i));
        }
    }

    @Test
    public void testShortLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getInt(i), b.getInt(i));
        }
    }

    @Test
    public void testIntLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.INT, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        IntLargeArray b = new IntLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getInt(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getInt(srcPos + i), b.getInt(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getInt(i));
        }

        b = new IntLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getIntData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getInt(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getInt(srcPos + i), b.getInt(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getInt(i));
        }

        int[] bb = new int[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new IntLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getInt(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getInt(srcPos + i), b.getInt(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getInt(i));
        }
    }

    @Test
    public void testIntLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.INT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.LONG);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getLong(i), b.getLong(i));
        }
    }

    @Test
    public void testLongLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LONG, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        LongLargeArray b = new LongLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getLong(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getLong(srcPos + i), b.getLong(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getLong(i));
        }

        b = new LongLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getLongData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getLong(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getLong(srcPos + i), b.getLong(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getLong(i));
        }

        long[] bb = new long[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new LongLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getLong(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getLong(srcPos + i), b.getLong(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getLong(i));
        }
    }

    @Test
    public void testLongLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.LONG, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.FLOAT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getFloat(i), b.getFloat(i), DELTA);
        }
    }

    @Test
    public void testFloatlargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        FloatLargeArray b = new FloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getFloat(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getFloat(srcPos + i), b.getFloat(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getFloat(i), DELTA);
        }

        b = new FloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getFloatData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getFloat(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getFloat(srcPos + i), b.getFloat(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getFloat(i), DELTA);
        }

        float[] bb = new float[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new FloatLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getFloat(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getFloat(srcPos + i), b.getFloat(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getFloat(i), DELTA);
        }
    }

    @Test
    public void testFloatLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.DOUBLE);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getDouble(i), b.getDouble(i), DELTA);
        }
    }

    @Test
    public void testDoubleLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 10);
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        DoubleLargeArray b = new DoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getDouble(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getDouble(srcPos + i), b.getDouble(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getDouble(i), DELTA);
        }

        b = new DoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getDoubleData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getDouble(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getDouble(srcPos + i), b.getDouble(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getDouble(i), DELTA);
        }

        double[] bb = new double[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new DoubleLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0, b.getDouble(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.getDouble(srcPos + i), b.getDouble(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0, b.getDouble(i), DELTA);
        }
    }

    @Test
    public void testDoubleLargeArrayConvert()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.FLOAT);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getDouble(i), b.getDouble(i), DELTA);
        }
    }

    @Test
    public void testComplexFloatLargeArrayArraycopy()
    {
        ComplexFloatLargeArray a = (ComplexFloatLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 10);
        int srcPos = 2;
        int destPos = 4;
        int length = (int) a.length() - 2;
        ComplexFloatLargeArray b = new ComplexFloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i), (float) DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertArrayEquals(a.getComplexFloat(srcPos + i), b.getComplexFloat(destPos + i), (float) DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i), (float) DELTA);
        }

        b = new ComplexFloatLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getComplexData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i), (float) DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertArrayEquals(a.getComplexFloat(srcPos / 2 + i), b.getComplexFloat(destPos + i), (float) DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i), (float) DELTA);
        }

        float[] bb = new float[4 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ComplexFloatLargeArray(bb);
        for (int i = 0; i < destPos / 2; i++) {
            assertArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i), (float) DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertArrayEquals(a.getComplexFloat(srcPos + i), b.getComplexFloat(destPos / 2 + i), (float) DELTA);
        }
        for (int i = destPos / 2 + length; i < b.length(); i++) {
            assertArrayEquals(new float[]{0f, 0f}, b.getComplexFloat(i), (float) DELTA);
        }
    }

    @Test
    public void testComplexFloatLargeArrayConvert()
    {
        ComplexFloatLargeArray a = (ComplexFloatLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.DOUBLE);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getComplexDouble(i)[0], b.getDouble(i), DELTA);
        }
    }

    @Test
    public void testComplexDoubleLargeArrayArraycopy()
    {
        ComplexDoubleLargeArray a = (ComplexDoubleLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_DOUBLE, 10);
        int srcPos = 2;
        int destPos = 4;
        int length = (int) a.length() - 2;
        ComplexDoubleLargeArray b = new ComplexDoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertArrayEquals(new double[]{0, 0}, b.getComplexDouble(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertArrayEquals(a.getComplexDouble(srcPos + i), b.getComplexDouble(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertArrayEquals(new double[]{0, 0}, b.getComplexDouble(i), DELTA);
        }

        b = new ComplexDoubleLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a.getComplexData(), srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertArrayEquals(new double[]{0, 0}, b.getComplexDouble(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertArrayEquals(a.getComplexDouble(srcPos / 2 + i), b.getComplexDouble(destPos + i), DELTA);
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertArrayEquals(new double[]{0, 0}, b.getComplexDouble(i), DELTA);
        }

        double[] bb = new double[4 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ComplexDoubleLargeArray(bb);
        for (int i = 0; i < destPos / 2; i++) {
            assertArrayEquals(new double[]{0, 0}, b.getComplexDouble(i), DELTA);
        }
        for (int i = 0; i < length; i++) {
            assertArrayEquals(a.getComplexDouble(srcPos + i), b.getComplexDouble(destPos / 2 + i), DELTA);
        }
        for (int i = destPos / 2 + length; i < b.length(); i++) {
            assertArrayEquals(new double[]{0, 0}, b.getComplexDouble(i), DELTA);
        }
    }

    @Test
    public void testComplexDoubleLargeArrayConvert()
    {
        ComplexDoubleLargeArray a = (ComplexDoubleLargeArray) LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_DOUBLE, 10);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.DOUBLE);
        for (int i = 0; i < a.length(); i++) {
            assertEquals(a.getComplexDouble(i)[0], b.getDouble(i), DELTA);
        }
    }

    @Test
    public void testStringLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.STRING, 10);
        String[] data = new String[(int) a.length()];
        for (int i = 0; i < data.length; i++) {
            data[i] = (String) a.get(i);
        }
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        StringLargeArray b = new StringLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        b = new StringLargeArray(2 * length);
        LargeArrayUtils.arraycopy(data, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        String[] bb = new String[2 * length];
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new StringLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }
    }

    @Test
    public void testStringLargeArrayConvert()
    {
        String[] data = new String[]{"a", "ab", "abc", "ąęć", "1234", "test string", "ANOTHER TEST STRING", "", "\n", "\r"};
        StringLargeArray a = new StringLargeArray(data);
        IntLargeArray b = (IntLargeArray) LargeArrayUtils.convert(a, LargeArrayType.INT);
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i].length(), b.getInt(i));
        }
    }

    @Test
    public void testObjectLargeArrayArraycopy()
    {
        LargeArray a = LargeArrayUtils.generateRandom(LargeArrayType.OBJECT, 10);
        Object[] data = new Object[(int) a.length()];
        for (int i = 0; i < data.length; i++) {
            data[i] = a.get(i);
        }
        int srcPos = 2;
        int destPos = 3;
        int length = (int) a.length() - 2;
        ObjectLargeArray b = new ObjectLargeArray(2 * length);
        LargeArrayUtils.arraycopy(a, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        b = new ObjectLargeArray(2 * length);
        LargeArrayUtils.arraycopy(data, srcPos, b, destPos, length);
        for (int i = 0; i < destPos; i++) {
            assertEquals(null, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(null, b.get(i));
        }

        Object[] bb = new Object[2 * length];
        for (int i = 0; i < bb.length; i++) {
            bb[i] = new Float(0);
        }
        LargeArrayUtils.arraycopy(a, srcPos, bb, destPos, length);
        b = new ObjectLargeArray(bb);
        for (int i = 0; i < destPos; i++) {
            assertEquals(0f, b.get(i));
        }
        for (int i = 0; i < length; i++) {
            assertEquals(a.get(srcPos + i), b.get(destPos + i));
        }
        for (int i = destPos + length; i < b.length(); i++) {
            assertEquals(0f, b.get(i));
        }
    }

    @Test
    public void testObjectLargeArrayConvert()
    {
        Object[] data = new Object[]{1.12345, -1.54321, 100., -100., Double.MAX_VALUE, Double.MIN_VALUE, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NaN, Double.MIN_NORMAL};
        ObjectLargeArray a = new ObjectLargeArray(data);
        LargeArray b = LargeArrayUtils.convert(a, LargeArrayType.STRING);
        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i].toString(), b.get(i));
        }
    }

    @Test
    public void testSelect()
    {
        double[] d = new double[]{1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10};
        byte[] m = new byte[]{0, 0, 1, 0, 1, 1, 1, 0, 0, 0};
        int length = 4;
        LargeArray data = new DoubleLargeArray(d);
        LogicLargeArray mask = new LogicLargeArray(m);
        LargeArray res = LargeArrayUtils.select(data, mask);
        assertEquals(length, res.length());
        assertEquals(d[2], res.getDouble(0), DELTA);
        assertEquals(d[4], res.getDouble(1), DELTA);
        assertEquals(d[5], res.getDouble(2), DELTA);
        assertEquals(d[6], res.getDouble(3), DELTA);
    }

    @Test
    public void testGenerateRandom()
    {

        int length = 10;
        LargeArray res = LargeArrayUtils.generateRandom(LargeArrayType.LOGIC, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.LOGIC, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.BYTE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.BYTE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.UNSIGNED_BYTE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.SHORT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.SHORT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.INT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.INT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.LONG, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.LONG, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.FLOAT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.DOUBLE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.COMPLEX_FLOAT, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_DOUBLE, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.COMPLEX_DOUBLE, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.STRING, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.STRING, res.getType());

        res = LargeArrayUtils.generateRandom(LargeArrayType.OBJECT, length);
        assertEquals(length, res.length());
        assertEquals(LargeArrayType.OBJECT, res.getType());
    }
}
