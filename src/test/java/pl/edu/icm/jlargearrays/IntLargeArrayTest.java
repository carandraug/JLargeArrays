/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jlargearrays;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class IntLargeArrayTest extends LargeArrayTest
{

    public IntLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testIntLargeArrayEqualsHashCode()
    {
        IntLargeArray a = new IntLargeArray(10);
        IntLargeArray b = new IntLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setInt(0, 1);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testIntLargeArrayApproximateHashCode()
    {
        IntLargeArray a = new IntLargeArray(10);
        IntLargeArray b = new IntLargeArray(10);
        a.setInt(0, 1);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new IntLargeArray(10, 0);
        b = new IntLargeArray(10, 1);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testIntLargeArrayGetSet()
    {
        IntLargeArray a = new IntLargeArray(10);
        long idx = 5;
        int val = -100;
        a.setInt(idx, val);
        assertEquals(val, a.getInt(idx));
        idx = 6;
        a.set(idx, val);
        assertEquals(val, (a.get(idx)).intValue());
    }

    @Test
    public void testIntLargeArrayGetSetNative()
    {
        IntLargeArray a = new IntLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            int val = -100;
            a.setToNative(idx, val);
            assertEquals(val, (int) a.getFromNative(idx));
        }
    }

    @Test
    public void testIntLargeArrayConstant()
    {
        IntLargeArray a = new IntLargeArray(1l << 33, 2);
        assertEquals(2, a.getInt(0));
        assertEquals(2, a.getInt(a.length() - 1));
        Throwable e = null;
        try {
            a.setInt(0, 3);
        } catch (IllegalAccessError ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalAccessError);
    }

    @Test
    public void testIntLargeArrayGetData()
    {
        int[] data = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        IntLargeArray a = new IntLargeArray(data);
        int[] res = a.getIntData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }
}
