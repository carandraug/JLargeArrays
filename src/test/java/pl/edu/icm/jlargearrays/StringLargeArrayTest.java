/* ***** BEGIN LICENSE BLOCK *****
 * JLargeArrays
 * Copyright (C) 2013 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jlargearrays;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class StringLargeArrayTest extends LargeArrayTest
{

    public StringLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testStringLargeArrayEqualsHashCode()
    {
        StringLargeArray a = new StringLargeArray(10);
        StringLargeArray b = new StringLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.set(0, "string");
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
        a.set(0, "");
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testStringLargeArrayApproximateHashCode()
    {
        StringLargeArray a = new StringLargeArray(10);
        StringLargeArray b = new StringLargeArray(10);
        a.set(0, "string");
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new StringLargeArray(10, "string1");
        b = new StringLargeArray(10, "string2");
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testStringLargeArrayConstant()
    {
        StringLargeArray a = new StringLargeArray(1l << 33, "test0123ąęćńżź");
        assertEquals("test0123ąęćńżź", a.get(0));
        assertEquals("test0123ąęćńżź", a.get(a.length() - 1));
        Throwable e = null;
        try {
            a.set(0, "test0123ąęćńżź");
        } catch (IllegalAccessError ex) {
            e = ex;
        }
        assertTrue(e instanceof IllegalAccessError);
    }

    @Test
    public void testStringLargeArrayGetSet()
    {
        StringLargeArray a = new StringLargeArray(10, 14);
        long idx = 5;
        String val1 = "test0123ąęćńżź";
        String val2 = "test";
        a.set(idx, val1);
        assertEquals(val1, a.get(idx));
        a.set(idx, val2);
        assertEquals(val2, a.get(idx));
    }

    @Test
    public void testStringLargeArrayGetSetNative()
    {
        StringLargeArray a = new StringLargeArray(10, 14);

        if (a.isLarge()) {
            long idx = 5;
            String val1 = "test0123ąęćńżź";
            String val2 = "test";
            a.setToNative(idx, val1);
            assertEquals(val1, a.getFromNative(idx));
            a.setToNative(idx, val2);
            assertEquals(val2, a.getFromNative(idx));
        }
    }
}
